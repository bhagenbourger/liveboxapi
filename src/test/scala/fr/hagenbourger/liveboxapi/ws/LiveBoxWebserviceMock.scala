package fr.hagenbourger.liveboxapi.ws

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpEntity, HttpHeader, HttpResponse, StatusCodes}
import fr.hagenbourger.liveboxapi.config.LiveBoxConf
import fr.hagenbourger.liveboxapi.model.{LiveBoxSession, LiveBoxSessionData, LiveBoxSessionWithCookie}
import org.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AsyncFlatSpec

import scala.concurrent.Future
import scala.io.{BufferedSource, Source}

trait LiveBoxWebserviceMock extends AsyncFlatSpec with MockitoSugar with BeforeAndAfterAll {

  val conf: LiveBoxConf = LiveBoxConf("", "", "", 1)
  private val cookie: String = "toto"
  private val headers: Seq[HttpHeader] = Seq(RawHeader("Set-Cookie", cookie))
  private val contextContent1: String =
    """{"status":0,"data":{"contextID":"myContextId1","username":"uname","groups":"http,admin"}}"""
  private val contextHttpResponse1: HttpResponse = HttpResponse(StatusCodes.OK, headers, HttpEntity(contextContent1))
  private val contextContent2: String =
    """{"status":0,"data":{"contextID":"myContextId2","username":"uname","groups":"http,admin"}}"""
  private val contextHttpResponse2: HttpResponse = HttpResponse(StatusCodes.OK, headers, HttpEntity(contextContent2))
  val expectedContext1: LiveBoxSessionWithCookie =
    LiveBoxSessionWithCookie(LiveBoxSession(LiveBoxSessionData("myContextId1", "http,admin", "uname"), 0), "toto")
  val expectedContext2: LiveBoxSessionWithCookie =
    LiveBoxSessionWithCookie(LiveBoxSession(LiveBoxSessionData("myContextId2", "http,admin", "uname"), 0), "toto")

  private val source: BufferedSource = Source.fromURL(getClass.getResource("/test.json"))
  private val devicesContent: String = source.mkString
  private val devicesHttpResponse: HttpResponse = HttpResponse(StatusCodes.OK, Seq.empty, devicesContent)

  val liveBoxWebservice: LiveBoxWebservice = mock[LiveBoxWebservice]

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    when(liveBoxWebservice.querySession)
      .thenReturn(
        Future {
          contextHttpResponse1
        },
        Future {
          contextHttpResponse2
        }
      )

    when(liveBoxWebservice.queryDevices(expectedContext1))
      .thenReturn(Future {
        devicesHttpResponse
      })

    when(liveBoxWebservice.queryDevices(expectedContext2))
      .thenReturn(Future {
        devicesHttpResponse
      })
  }
}
