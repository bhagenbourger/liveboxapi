package fr.hagenbourger.liveboxapi.ws

import fr.hagenbourger.liveboxapi.ws.LiveBoxWebservice.{getSessionBody, getXSah}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class LiveBoxWebserviceSpec extends AnyFlatSpec with should.Matchers {

  "Webservice" should "compute session body" in {
    val body: String = getSessionBody("toto", "pwd")
    assert(body == "{\"service\": \"sah.Device.Information\",\"method\": \"createContext\",\"parameters\": {\"applicationName\": \"webui\",\"username\": \"toto\",\"password\": \"pwd\"}}")
  }

  it should "compute x-sah header" in {
    val xSah: String = getXSah("xxxx")
    assert(xSah == "X-Sah xxxx")
  }
}
