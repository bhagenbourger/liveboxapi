package fr.hagenbourger.liveboxapi.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import fr.hagenbourger.liveboxapi.repository.LiveBoxRepository
import fr.hagenbourger.liveboxapi.ws.LiveBoxWebserviceMock
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

class LiveBoxControllerSpec extends LiveBoxWebserviceMock with ScalatestRouteTest {

  private val controller = new LiveBoxController(new LiveBoxRepository(liveBoxWebservice, conf))
  private val expectedSessionResponse1: String =
    """{""" +
      """"cookie":"toto",""" +
      """"liveBoxSession":{""" +
      """"data":{"contextID":"myContextId1","groups":"http,admin","username":"uname"},""" +
      """"status":0""" +
      """}}""".stripMargin
  private val expectedSessionResponse2: String =
    """{""" +
      """"cookie":"toto",""" +
      """"liveBoxSession":{""" +
      """"data":{"contextID":"myContextId2","groups":"http,admin","username":"uname"},""" +
      """"status":0""" +
      """}}""".stripMargin
  private val expectedDevicesResponse: String =
    """[""" +
      """{"DeviceType":"","FirstSeen":"2021-10-21T01:15:27Z","IPAddress":"192.168.1.1","LastChanged":"2021-10-21T01:26:18Z","LastConnection":"2021-10-21T01:15:27Z","Name":"lan"},""" +
      """{"FirstSeen":"2021-10-21T01:15:26Z","LastChanged":"2021-11-05T19:53:31Z","LastConnection":"2021-10-21T01:16:18Z","Name":"wl0"},""" +
      """{"DeviceType":"","FirstSeen":"2021-10-21T01:15:27Z","LastChanged":"2021-11-05T20:18:37Z","LastConnection":"2021-10-21T01:16:16Z","Name":"eth4"},""" +
      """{"DeviceType":"crm","FirstSeen":"2021-10-21T01:15:25Z","IPAddress":"192.168.1.11","InterfaceType":"WiFi","LastChanged":"2021-11-05T03:54:18Z","LastConnection":"2021-11-05T03:54:17Z","Name":"device-one","PhysAddress":"00:14:22:01:23:45"},""" +
      """{"DeviceType":"rwi","FirstSeen":"2021-10-21T01:15:26Z","IPAddress":"192.168.1.24","LastChanged":"2021-11-05T21:19:51Z","LastConnection":"2021-11-05T16:58:42Z","Name":"rwi"},""" +
      """{"DeviceType":"","FirstSeen":"2021-10-21T01:18:50Z","LastChanged":"2021-10-21T01:18:52Z","LastConnection":"2021-10-21T01:18:50Z","Name":"crm"},""" +
      """{"DeviceType":"","FirstSeen":"2021-10-21T01:16:10Z","LastChanged":"2021-11-05T17:00:04Z","LastConnection":"2021-10-21T01:16:10Z","Name":"PC-33","PhysAddress":"00:14:22:01:23:AB"}""" +
      """]""".stripMargin
  private val expectedHealthCheckResponse = """{"status":"OK"}"""

  "LiveBox controller" should "have health check ok" in {
    Get("/healthcheck") ~> controller.routes ~> check {
      status shouldEqual StatusCodes.OK
      responseAs[String] shouldEqual expectedHealthCheckResponse
    }
  }

  it should "get session" in {
    Get("/session") ~> controller.routes ~> check {
      status shouldEqual StatusCodes.OK
      responseAs[String] shouldEqual expectedSessionResponse1
    }

    Get("/session") ~> controller.routes ~> check {
      status shouldEqual StatusCodes.OK
      responseAs[String] shouldEqual expectedSessionResponse2
    }
  }

  it should "get devices" in {
    Get("/devices") ~> controller.routes ~> check {
      status shouldEqual StatusCodes.OK
      responseAs[String] shouldEqual expectedDevicesResponse
    }
  }

}
