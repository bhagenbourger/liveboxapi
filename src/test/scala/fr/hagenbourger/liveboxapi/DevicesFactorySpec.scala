package fr.hagenbourger.liveboxapi

import fr.hagenbourger.liveboxapi.model.Device
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.io.{BufferedSource, Source}

class DevicesFactorySpec extends AnyFlatSpec with should.Matchers {

  "Device list" should "be built from json" in {
    val source: BufferedSource = Source.fromURL(getClass.getResource("/test.json"))
    val json: Seq[Device] = DevicesFactory.fromJson(source.mkString)

    assert(json == DevicesFactorySpec.expectedDevices)
  }
}

object DevicesFactorySpec {
  val expectedDevices: Seq[Device] = Seq(
    Device(
      "lan",
      Some(""),
      "2021-10-21T01:15:27Z",
      "2021-10-21T01:15:27Z",
      "2021-10-21T01:26:18Z",
      Some("192.168.1.1"),
      None,
      None
    ),
    Device("wl0",
      None,
      "2021-10-21T01:15:26Z",
      "2021-10-21T01:16:18Z",
      "2021-11-05T19:53:31Z",
      None,
      None,
      None
    ),
    Device("eth4",
      Some(""),
      "2021-10-21T01:15:27Z",
      "2021-10-21T01:16:16Z",
      "2021-11-05T20:18:37Z",
      None,
      None,
      None
    ),
    Device(
      "device-one",
      Some("crm"),
      "2021-10-21T01:15:25Z",
      "2021-11-05T03:54:17Z",
      "2021-11-05T03:54:18Z",
      Some("192.168.1.11"),
      Some("00:14:22:01:23:45"),
      Some("WiFi")
    ),
    Device(
      "rwi",
      Some("rwi"),
      "2021-10-21T01:15:26Z",
      "2021-11-05T16:58:42Z",
      "2021-11-05T21:19:51Z",
      Some("192.168.1.24"),
      None,
      None
    ),
    Device(
      "crm",
      Some(""),
      "2021-10-21T01:18:50Z",
      "2021-10-21T01:18:50Z",
      "2021-10-21T01:18:52Z",
      None,
      None,
      None
    ),
    Device(
      "PC-33",
      Some(""),
      "2021-10-21T01:16:10Z",
      "2021-10-21T01:16:10Z",
      "2021-11-05T17:00:04Z",
      None,
      Some("00:14:22:01:23:AB"),
      None
    )
  )
}
