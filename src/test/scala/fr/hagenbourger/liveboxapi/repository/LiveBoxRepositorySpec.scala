package fr.hagenbourger.liveboxapi.repository

import fr.hagenbourger.liveboxapi.DevicesFactorySpec
import fr.hagenbourger.liveboxapi.model.Device
import fr.hagenbourger.liveboxapi.ws.LiveBoxWebserviceMock

import scala.concurrent.Future

class LiveBoxRepositorySpec extends LiveBoxWebserviceMock {

  "Repository" should "get livebox session" in {
    val liveBoxRepository: LiveBoxRepository = new LiveBoxRepository(liveBoxWebservice, conf)

    for {
      context1 <- liveBoxRepository.getLiveBoxSession()
      context1Bis <- liveBoxRepository.getLiveBoxSession()
      context2 <- liveBoxRepository.getLiveBoxSession(clear = true)
    } yield {
      assert(context1 == expectedContext1)
      assert(context1Bis == expectedContext1)
      assert(context2 == expectedContext2)
    }
  }

  it should "get devices" in {
    val context: Future[Seq[Device]] = new LiveBoxRepository(liveBoxWebservice, conf).getDevices

    context map { c => assert(c == DevicesFactorySpec.expectedDevices) }
  }

}
