package fr.hagenbourger.liveboxapi

import com.typesafe.config.ConfigFactory
import fr.hagenbourger.liveboxapi.config.LiveBoxConf
import fr.hagenbourger.liveboxapi.controller.LiveBoxController
import fr.hagenbourger.liveboxapi.repository.LiveBoxRepository
import fr.hagenbourger.liveboxapi.ws.LiveBoxWebservice

trait Module {
  lazy val conf: LiveBoxConf = LiveBoxConf(ConfigFactory.load())
  lazy val liveBoxWebservice: LiveBoxWebservice = new LiveBoxWebservice(conf)
  lazy val liveBoxRepository: LiveBoxRepository = new LiveBoxRepository(liveBoxWebservice, conf)
  lazy val liveBoxController: LiveBoxController = new LiveBoxController(liveBoxRepository)
}
