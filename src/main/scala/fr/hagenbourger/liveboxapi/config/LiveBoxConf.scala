package fr.hagenbourger.liveboxapi.config

import com.typesafe.config.Config

case class LiveBoxConf(
                        endpoint: String,
                        username: String,
                        password: String,
                        cacheSessionExpirationInMinutes: Int
                      )

object LiveBoxConf {

  private val endpointKey: String = "livebox.endpoint"
  private val usernameKey: String = "livebox.username"
  private val passwordKey: String = "livebox.password"
  private val cacheSessionKey: String = "cache.session-expiration-in-minutes"

  def apply(conf: Config): LiveBoxConf = LiveBoxConf(
    conf.getString(endpointKey),
    conf.getString(usernameKey),
    conf.getString(passwordKey),
    conf.getInt(cacheSessionKey)
  )
}
