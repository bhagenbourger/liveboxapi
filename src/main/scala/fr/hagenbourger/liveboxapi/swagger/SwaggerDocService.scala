package fr.hagenbourger.liveboxapi.swagger

import com.github.swagger.akka.model.Info
import com.github.swagger.akka.ui.SwaggerHttpWithUiService
import fr.hagenbourger.liveboxapi.controller.LiveBoxController

object SwaggerDocService extends SwaggerHttpWithUiService {
  override val apiClasses: Set[Class[_]] = Set(classOf[LiveBoxController])
  override val host: String = "127.0.0.1:8080"
  override val info: Info = Info(
    description = "API to query your Orange Livebox v5",
    version = "0.1",
    title = "Livebox API"
  )
}
