package fr.hagenbourger.liveboxapi.model

final case class LiveBoxSessionWithCookie(liveBoxSession: LiveBoxSession, cookie: String)
