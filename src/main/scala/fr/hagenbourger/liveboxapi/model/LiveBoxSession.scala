package fr.hagenbourger.liveboxapi.model

final case class LiveBoxSession(data: LiveBoxSessionData, status: Int)
