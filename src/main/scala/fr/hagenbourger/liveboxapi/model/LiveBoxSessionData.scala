package fr.hagenbourger.liveboxapi.model

final case class LiveBoxSessionData(contextID: String, groups: String, username: String)
