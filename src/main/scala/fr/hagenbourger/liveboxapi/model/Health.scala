package fr.hagenbourger.liveboxapi.model

final case class Health(status: String, currentSession: Option[LiveBoxSessionWithCookie])
