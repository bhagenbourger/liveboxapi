package fr.hagenbourger.liveboxapi.model

final case class Device(
                         Name: String,
                         DeviceType: Option[String],
                         FirstSeen: String,
                         LastConnection: String,
                         LastChanged: String,
                         IPAddress: Option[String],
                         PhysAddress: Option[String],
                         InterfaceType: Option[String]
                       )
