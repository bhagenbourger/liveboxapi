package fr.hagenbourger.liveboxapi.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

object JsonFormat extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val liveBoxSessionDataFormat: RootJsonFormat[LiveBoxSessionData] = jsonFormat3(LiveBoxSessionData)
  implicit val liveBoxSessionFormat: RootJsonFormat[LiveBoxSession] = jsonFormat2(LiveBoxSession)
  implicit val liveBoxSessionWithCookieFormat: RootJsonFormat[LiveBoxSessionWithCookie] = jsonFormat2(LiveBoxSessionWithCookie)
  implicit val deviceFormat: RootJsonFormat[Device] = jsonFormat8(Device)
  implicit val healthFormat: RootJsonFormat[Health] = jsonFormat2(Health)
}
