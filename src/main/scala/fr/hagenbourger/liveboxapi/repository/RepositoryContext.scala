package fr.hagenbourger.liveboxapi.repository

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.ExecutionContextExecutor

object RepositoryContext {

  implicit val actorSystem: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "RepositoryContext")
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext
}
