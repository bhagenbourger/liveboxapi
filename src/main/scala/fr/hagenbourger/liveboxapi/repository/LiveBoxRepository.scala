package fr.hagenbourger.liveboxapi.repository

import akka.http.scaladsl.model._
import fr.hagenbourger.liveboxapi.DevicesFactory
import fr.hagenbourger.liveboxapi.config.LiveBoxConf
import fr.hagenbourger.liveboxapi.model.{Device, LiveBoxSession, LiveBoxSessionWithCookie}
import fr.hagenbourger.liveboxapi.repository.LiveBoxRepository._
import fr.hagenbourger.liveboxapi.ws.LiveBoxWebservice
import spray.json._

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object LiveBoxRepository {

  private[repository] def getCookie(response: HttpResponse): String =
    response.headers.filter(_.lowercaseName().equals("set-cookie")).map(_.value()).map(_.split(";").head).head
}

class LiveBoxRepository(liveBoxWebservice: LiveBoxWebservice, liveBoxConf: LiveBoxConf) {

  import RepositoryContext._
  import fr.hagenbourger.liveboxapi.model.JsonFormat._

  val currentSession = new SessionCache(liveBoxConf)

  def getLiveBoxSession(clear: Boolean = false): Future[LiveBoxSessionWithCookie] = {
    if (clear) {
      currentSession.clearCache()
    }

    currentSession
      .get
      .map(s => Future {
        s
      })
      .getOrElse(
        liveBoxWebservice.querySession
          .flatMap {
            case response@HttpResponse(StatusCodes.OK, _, _, _) =>
              val cookie: String = getCookie(response)
              response
                .entity
                .toStrict(5 seconds)
                .map(_.data.decodeString(HttpCharsets.`UTF-8`.value))
                .map(_.parseJson.convertTo[LiveBoxSession])
                .map(LiveBoxSessionWithCookie(_, cookie))
                .map(currentSession.set)
          }
      )
  }

  def getDevices: Future[Seq[Device]] = getLiveBoxSession().flatMap(getDevices)

  private def getDevices(context: LiveBoxSessionWithCookie): Future[Seq[Device]] = {
    liveBoxWebservice.queryDevices(context)
      .flatMap(
        _.entity
          .toStrict(5 seconds)
          .map(_.data.decodeString(HttpCharsets.`UTF-8`.value))
          .map(DevicesFactory.fromJson)
      )
  }
}
