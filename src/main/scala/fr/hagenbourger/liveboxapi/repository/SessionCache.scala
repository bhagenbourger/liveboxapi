package fr.hagenbourger.liveboxapi.repository

import com.github.blemale.scaffeine.{Cache, Scaffeine}
import fr.hagenbourger.liveboxapi.config.LiveBoxConf
import fr.hagenbourger.liveboxapi.model.LiveBoxSessionWithCookie

import scala.concurrent.duration.DurationInt

class SessionCache(liveBoxConf: LiveBoxConf) {
  val sessionIndex: Int = 1
  val cache: Cache[Int, LiveBoxSessionWithCookie] =
    Scaffeine()
      .recordStats()
      .expireAfterAccess(liveBoxConf.cacheSessionExpirationInMinutes.minute)
      .maximumSize(1)
      .build[Int, LiveBoxSessionWithCookie]()

  def clearCache(): Unit = cache.invalidateAll()

  def set(session: LiveBoxSessionWithCookie): LiveBoxSessionWithCookie = {
    cache.put(sessionIndex, session)
    session
  }

  def get: Option[LiveBoxSessionWithCookie] = cache.getIfPresent(sessionIndex)
}
