package fr.hagenbourger.liveboxapi

import fr.hagenbourger.liveboxapi.model.Device
import net.liftweb.json.JsonAST.JArray
import net.liftweb.json.{DefaultFormats, JsonParser}

object DevicesFactory {
  def fromJson(json: String): Seq[Device] = {
    implicit val formats: DefaultFormats = DefaultFormats
    val js = JsonParser.parse(json)
    (js \\ "Children")
      .children
      .flatMap {
        case a: JArray => a.children.map(_.extract[Device])
        case s => Seq(s.extract[Device])
      }
  }
}
