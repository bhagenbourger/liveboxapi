package fr.hagenbourger.liveboxapi.ws

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import fr.hagenbourger.liveboxapi.config.LiveBoxConf
import fr.hagenbourger.liveboxapi.model.LiveBoxSessionWithCookie
import fr.hagenbourger.liveboxapi.ws.LiveBoxWebservice._

import scala.concurrent.Future

object LiveBoxWebservice {
  private val contentType: ContentType.WithFixedCharset =
    ContentType(MediaType.customWithFixedCharset("application", "x-sah-ws-4-call+json", HttpCharsets.`UTF-8`))
  private val sahLoginHeader: RawHeader = RawHeader("Authorization", "X-Sah-Login")
  private val deviceBody: String =
    "{\"service\": \"TopologyDiagnostics\",\"method\": \"buildTopology\",\"parameters\": {\"SendXmlFile\": false}}"
  private val xContextHeader: String = "X-Context"
  private val authorizationHeader: String = "Authorization"
  private val cookieHeader: String = "Cookie"
  private val xSah: String = "X-Sah "

  private[ws] def getXSah(contextID: String): String = xSah + contextID

  private[ws] def getSessionBody(username: String, password: String) =
    s"{\"service\": \"sah.Device.Information\"," +
      s"\"method\": \"createContext\"," +
      s"\"parameters\": {\"applicationName\": \"webui\"," +
      s"\"username\": \"$username\"," +
      s"\"password\": \"$password\"}}"
}

class LiveBoxWebservice(liveBoxConf: LiveBoxConf) {

  import WebserviceContext._

  def querySession: Future[HttpResponse] = {
    Http()
      .singleRequest(HttpRequest(
        method = HttpMethods.POST,
        uri = liveBoxConf.endpoint,
        entity = HttpEntity(contentType, getSessionBody(liveBoxConf.username, liveBoxConf.password)),
        headers = Seq(sahLoginHeader)
      ))
  }

  def queryDevices(context: LiveBoxSessionWithCookie): Future[HttpResponse] = {
    Http()
      .singleRequest(
        HttpRequest(
          method = HttpMethods.POST,
          uri = liveBoxConf.endpoint,
          entity = HttpEntity(contentType, deviceBody),
          headers = Seq(
            RawHeader(xContextHeader, context.liveBoxSession.data.contextID),
            RawHeader(authorizationHeader, getXSah(context.liveBoxSession.data.contextID)),
            RawHeader(cookieHeader, context.cookie)
          )
        )
      )
  }
}
