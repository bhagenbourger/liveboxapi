package fr.hagenbourger.liveboxapi.ws

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.ExecutionContextExecutor

object WebserviceContext {

  implicit val actorSystem: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "WebserviceContext")
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext
}
