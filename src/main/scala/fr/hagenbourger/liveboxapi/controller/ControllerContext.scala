package fr.hagenbourger.liveboxapi.controller

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.ExecutionContextExecutor

object ControllerContext {
  implicit val actorSystem: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "ControllerContext")
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext
}
