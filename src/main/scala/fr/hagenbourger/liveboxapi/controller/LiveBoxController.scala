package fr.hagenbourger.liveboxapi.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server
import akka.http.scaladsl.server.{Directives, Route}
import fr.hagenbourger.liveboxapi.model.{Device, Health, LiveBoxSessionWithCookie}
import fr.hagenbourger.liveboxapi.repository.LiveBoxRepository
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.{GET, Path, Produces}

import scala.util.{Failure, Success}

class LiveBoxController(liveBoxRepository: LiveBoxRepository) extends Directives {

  import fr.hagenbourger.liveboxapi.model.JsonFormat._

  val routes: server.Route = healthCheckRoute ~ sessionRoute ~ devicesRoute

  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Path("/healthcheck")
  @Tag(name = "monitoring")
  @Operation(
    summary = "Returns the health status of the API",
    description = "Returns the health status of the API",
    responses = Array(new ApiResponse(
      responseCode = "200",
      description = "Health check response",
      content = Array(new Content(schema = new Schema(implementation = classOf[Health])))
    ))
  )
  def healthCheckRoute: Route = path("healthcheck") {
    get {
      complete(StatusCodes.OK, Health(StatusCodes.OK.reason, liveBoxRepository.currentSession.get))
    }
  }

  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Path("/session")
  @Tag(name = "monitoring")
  @Operation(
    summary = "Returns a new Livebox session",
    description = "Returns a new Livebox session including the username, the contextID and the session cookie",
    responses = Array(new ApiResponse(
      responseCode = "200",
      description = "Session response",
      content = Array(new Content(schema = new Schema(implementation = classOf[LiveBoxSessionWithCookie])))
    ))
  )
  def sessionRoute: Route = {
    path("session") {
      get {
        onComplete(liveBoxRepository.getLiveBoxSession(clear = true)) {
          case Success(session) => complete(StatusCodes.OK, session)
          case Failure(ex) => complete(ex.toString)
        }
      }
    }
  }

  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Path("/devices")
  @Tag(name = "devices")
  @Operation(
    summary = "Returns the list of devices connected to the Livebox",
    description = "Returns the list of devices connected to the Livebox including their name and their IP",
    responses = Array(new ApiResponse(
      responseCode = "200",
      description = "Devices response",
      content = Array(new Content(schema = new Schema(implementation = classOf[List[Device]])))
    ))
  )
  def devicesRoute: Route = {
    path("devices") {
      get {
        onComplete(liveBoxRepository.getDevices) {
          case Success(devices) => complete(StatusCodes.OK, devices)
          case Failure(ex) => complete(ex.toString)
        }
      }
    }
  }
}
