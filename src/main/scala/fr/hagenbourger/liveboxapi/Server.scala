package fr.hagenbourger.liveboxapi

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import fr.hagenbourger.liveboxapi.swagger.SwaggerDocService
import akka.http.scaladsl.server.Directives._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.language.postfixOps

object Server extends App with Module {

  implicit val actorSystem: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "my-system")
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext

  val bindingFuture = Http()
    .newServerAt("0.0.0.0", 8080)
    .bind(liveBoxController.routes ~ SwaggerDocService.routes)
  Await.result(actorSystem.whenTerminated, Duration.Inf)
}
