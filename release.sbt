import sbtrelease.ReleaseStateTransformations._
import sbtrelease.{Version, versionFormatError}

def getVersionFromTag(extracted: Extracted, bump: Version.Bump): String = {
  extracted.get(releaseVcs)
    .map(_.cmd("describe", "--always").!!.trim.substring(1))
    .map(ver => Version(ver)
      .orElse(Version("0.0.0"))
      .map(_.withoutQualifier)
      .map(_.bump(bump).string)
      .getOrElse(versionFormatError(ver))
    )
    .getOrElse("Error getting version from tag")
}

def executeRelease(state: State, bump: Version.Bump): State = {
  val extracted: Extracted = Project extract state
  val customState: State = extracted.appendWithSession(
    Seq(releaseVersion := { _ => getVersionFromTag(extracted, bump) }),
    state
  )
  Command.process("release with-defaults", customState)
}

commands += Command.command("majorRelease")((state: State) => {
  executeRelease(state, sbtrelease.Version.Bump.Major)
})

commands += Command.command("minorRelease")((state: State) => {
  executeRelease(state, sbtrelease.Version.Bump.Minor)
})

commands += Command.command("patchRelease")((state: State) => {
  executeRelease(state, sbtrelease.Version.Bump.Bugfix)
})

dockerRepository in ThisBuild := sys.env.get("CI_REGISTRY_IMAGE")
releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  setReleaseVersion,
  tagRelease,
  releaseStepTask(Docker / publish),
  pushChanges
)