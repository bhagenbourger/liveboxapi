# Livebox API
API to query your Orange Livebox v5

# Endpoints
You can use the swagger endpoint to view the list of available endpoints : http://127.0.0.1:8080/api-docs/

![img.png](images/swagger.png)

# Run
Set environment variables
```bash
LIVEBOX_USERNAME=<YOUR LIVEBOX USERNAME>
LIVEBOX_PASSWORD=<YOUR LIVEBOX PASSWORD>
```

Run docker  
```bash
docker run -p 127.0.0.1:8080:8080 -e LIVEBOX_USERNAME=$LIVEBOX_USERNAME -e LIVEBOX_PASSWORD=$LIVEBOX_PASSWORD liveboxapi:0.1
```