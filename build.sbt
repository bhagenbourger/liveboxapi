enablePlugins(JavaAppPackaging, AshScriptPlugin)

name := "liveboxapi"
organization := "fr.hagenbourger"

scalaVersion := "2.13.6"

val AkkaVersion = "2.6.18"
val AkkaHttpVersion = "10.2.6"

val swaggerDependencies = Seq(
  "jakarta.ws.rs" % "jakarta.ws.rs-api" % "3.0.0",
  "com.github.swagger-akka-http" %% "swagger-akka-http-with-ui" % "2.6.0",
  "com.github.swagger-akka-http" %% "swagger-scala-module" % "2.5.2",
  "com.github.swagger-akka-http" %% "swagger-enumeratum-module" % "2.3.0",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.1",
  "io.swagger.core.v3" % "swagger-jaxrs2-jakarta" % "2.1.12"
)

val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.2.10" % "test",
  "org.mockito" %% "mockito-scala" % "1.16.49" % "test",
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion % "test",
  "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion % "test"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "net.liftweb" %% "lift-json" % "3.5.0",
  "com.typesafe" % "config" % "1.4.1",
  "com.github.blemale" %% "scaffeine" % "5.1.2"
) ++ swaggerDependencies ++ testDependencies

dockerBaseImage := "openjdk:11-jre-slim"
dockerExposedPorts := Seq(8080)
